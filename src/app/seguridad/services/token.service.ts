import { Injectable } from '@angular/core';


const TOKEN_KEY = 'AuthToken';
const USERNAME_KEY = 'AuthUsername';
const AUTHORITIES_KEY = 'AuthAuthorities';

@Injectable({
  providedIn: 'root'
})
export class TokenService {

  roles: Array<String> = [];

  constructor() { }

  public setToken(token:string): void{
    window.sessionStorage.removeItem(TOKEN_KEY);
    window.sessionStorage.setItem(TOKEN_KEY,token);
  }

  public getToken():string|any{
    return sessionStorage.getItem(TOKEN_KEY);
  }

  public setUserName(userName:string): void{
    window.sessionStorage.removeItem(USERNAME_KEY);
    window.sessionStorage.setItem(USERNAME_KEY,userName);
  }

  public getUsername():string|any{
    return sessionStorage.getItem(USERNAME_KEY);
  }

  public setAuthorities(authorities:string[]): void{
    window.sessionStorage.removeItem(AUTHORITIES_KEY);
    window.sessionStorage.setItem(AUTHORITIES_KEY,JSON.stringify(authorities));
  }

  public getAuthorities(): String[] {
    this.roles = [];
    const storedAuthorities = sessionStorage.getItem(AUTHORITIES_KEY);
    if (storedAuthorities) {
      const parsedAuthorities = JSON.parse(storedAuthorities);
      if (Array.isArray(parsedAuthorities)) {
        parsedAuthorities.forEach((authorityObject: { authority: string }) => {
          this.roles.push(authorityObject.authority);
        });
      }
    }
    return this.roles;
  }

  public logout():void{
    window.sessionStorage.clear();
  }



}
