import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { TokenService } from './token.service';

@Injectable({
  providedIn: 'root'
})
export class ProdGuardService implements CanActivate {

  constructor(private tokenService: TokenService, private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
    const expectedRoles = route.data['expectedRoles'];
    const roles = this.tokenService.getAuthorities();

    if (!roles || roles.length === 0) {
      this.router.navigate(['/login']);
      return false;
    }

    const hasRole = roles.some(role => expectedRoles.includes(role));
    if (!this.tokenService.getToken() || !hasRole) {
      this.router.navigate(['/login']);
      return false;
    }
    return true;
  }
}