import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { TokenService } from './token.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private tokenService: TokenService, private router: Router) {}

  canActivate(): boolean {
    if (this.tokenService.getToken()) {
      // Aquí puedes redirigir según el rol del usuario
      const roles = this.tokenService.getAuthorities();
      if (roles.includes('ROLE_ADMIN')) {
        this.router.navigate(['/personas']); // Redirige al módulo de admin
      } else {
        this.router.navigate(['/personas']); // Redirige al módulo de usuarios
      }
      return false;
    }
    return true;
  }
}
