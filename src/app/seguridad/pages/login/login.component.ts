import { Component, OnInit } from '@angular/core';
import { TokenService } from '../../services/token.service';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { LoginUsuario } from '../../interfaces/login-usuario';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  form!: FormGroup;
  roles!:String[];
  hidePassword = true;
  isLogged = false;
  isLoginFail = false;

  constructor(
    private tokenService:TokenService,
    private authService:AuthService,
    private fb: FormBuilder, 
    private router: Router,
    private _snackBar: MatSnackBar
  ){

  }
  ngOnInit(): void {
    this.form = this.fb.group({
      nombreUsuario: ['', Validators.required],
      password: ['', Validators.required],
    })

    if(this.tokenService.getToken()){
      this.isLogged = true;
      this.isLoginFail = false;
      this.roles = this.tokenService.getAuthorities();
    }
  }

  onLogin():void{
    const usuario: LoginUsuario = this.form.value;
    this.authService.login(usuario).subscribe((data)=>{
      this.isLogged=true;
      this.isLoginFail = false;
      this.tokenService.setToken(data.token);
      this.tokenService.setUserName(data.nombreUsuario);
      this.tokenService.setAuthorities(data.authorities);
      this.roles = data.authorities;
      this.openSnackBar("Inicio de Sesion Exitosa","aceptar");
      this.router.navigate(["personas"]);
    
    },err=>{
      this.isLogged = false;
      this.isLoginFail = true;
      this.openSnackBar("Fallo el inicio de sesion","aceptar");
      console.log(err.error.mensaje);
    })
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action,{
      duration: 3000
    });
  }
}
