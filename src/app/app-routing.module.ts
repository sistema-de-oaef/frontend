import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProdGuardService } from './seguridad/services/prod-guard.service';
import { AuthGuard } from './seguridad/services/auth-guard.service';

const routes: Routes = [
  {
    path:'login',
    loadChildren: ()=>import('./seguridad/seguridad.module').then(m=>m.SeguridadModule), canActivate: [AuthGuard]
  },
  {
    path:'personas',
    loadChildren: ()=>import('./personas/personas.module').then(m=>m.PersonasModule),canActivate:[ProdGuardService],data: { expectedRoles: ['ROLE_ADMIN']}
  },
  {
    path:'',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'login/**',
    redirectTo: 'login'
  },
  {
    path: '**',
    redirectTo: 'login'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
