import { Injectable } from '@angular/core';
import { enviroment } from 'src/enviroments/enviroment';
import {HttpClient} from '@angular/common/http'
import { Observable, map } from 'rxjs';
import { BuscarPersonaResponseModel } from '../interfaces/buscar-persona-response-model';
import { RegistrarPersonaRequestModel } from '../interfaces/RegistrarPersonaRequestModel.interface';
import { ActualizarPersonaRequestModel } from '../interfaces/actualizar-persona-request-model';
import { NuevoUsuario } from '../interfaces/nuevo-usuario';

@Injectable({
  providedIn: 'root'
})
export class PersonasService {

  private url = "http://localhost:8080/usuario/";

  constructor(private http:HttpClient) {
  }

  buscarPersonas():Observable<BuscarPersonaResponseModel[]>{
    return this.http.get<BuscarPersonaResponseModel[]>(this.url + 'lista');
  }

  buscarPersona(id: number): Observable<BuscarPersonaResponseModel> {
    return this.http.get<any>(this.url + "detalle/" + id)
      .pipe(
        map((response: any) => {
          return {
            id: response.id,
            nombre: response.nombre,
            nombreUsuario: response.nombreUsuario,
            email: response.email,
            password: response.password,
            roles: response.roles.map((rol: any) => {
              return {
                id: rol.id,
                rolNombre: rol.rolNombre
              };
            })
          };
        })
      );
  }

  registrarPersona(usuario: NuevoUsuario): Observable<any> {
    return this.http.post(this.url + 'registrar', usuario, { responseType: 'text' });
  }

  actualizarPersona(id:number,persona:ActualizarPersonaRequestModel):Observable<any>{
    return this.http.put(this.url + "actualizar/"+id,persona, { responseType: 'text' });
  }

  eliminarPersona(id:number):Observable<boolean>{
    return this.http.delete<boolean>(this.url + "eliminar/" + id);
  }

}
