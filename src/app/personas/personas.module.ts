import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PersonasRoutingModule } from './personas-routing.module';
import { ListadoComponent } from './pages/listado/listado.component';
import { RegistrarComponent } from './pages/registrar/registrar.component';
import { MaterialModule } from '../material/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ModificarComponent } from './pages/modificar/modificar.component';
import {ConfirmDialogComponent} from './pages/confirm-dialog/confirm-dialog.component';
import { VerDetallesComponent } from './pages/ver-detalles/ver-detalles.component';
import { PrincipalComponent } from './pages/principal/principal.component';


@NgModule({
  declarations: [
    ListadoComponent,
    RegistrarComponent,
    ModificarComponent,
    ConfirmDialogComponent,
    VerDetallesComponent,
    PrincipalComponent
  ],
  imports: [
    CommonModule,
    PersonasRoutingModule,
    MaterialModule,
    ReactiveFormsModule
  ]
})
export class PersonasModule { }
