export interface ActualizarPersonaRequestModel {
    nombre:string,
    nombreUsuario:string,
    email:string,
    roles:string[]
}
