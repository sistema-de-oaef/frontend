export interface BuscarPersonaResponseModel {
    id: number;
    nombre: string;
    nombreUsuario: string;
    email: string;
    password: string; // Agrega la propiedad password
    roles: Rol[];
  }
  
  export interface Rol {
    id: number;
    rolNombre: string;
  }