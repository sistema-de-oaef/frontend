export interface RegistrarPersonaRequestModel{
    nombre:string,
    apellido:string,
    dni:string,
    fechaNacimiento:Date
}