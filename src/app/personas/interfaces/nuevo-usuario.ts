export interface NuevoUsuario {
    nombre: String;
    nombreUsuario: String;
    email: String;
    password: String;
    roles: String[];
}
