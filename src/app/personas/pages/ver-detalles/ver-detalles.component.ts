import { HttpClient } from '@angular/common/http';
import { Component, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PersonasService } from '../../services/personas.service';
import { Router } from '@angular/router';
import { ModificarComponent } from '../modificar/modificar.component';
import { MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BuscarPersonaResponseModel } from '../../interfaces/buscar-persona-response-model';

@Component({
  selector: 'app-ver-detalles',
  templateUrl: './ver-detalles.component.html',
  styleUrls: ['./ver-detalles.component.css']
})
export class VerDetallesComponent {

  @Input() id!:number;

  form!: FormGroup;

  constructor(private http: HttpClient, private personaService: PersonasService, private fb: FormBuilder, private router: Router,private dialogRef: MatDialogRef<ModificarComponent>,private _snackBar: MatSnackBar) { }

  ngOnInit(): void{
    this.form = this.fb.group({
      id:0,
      nombre: ['', Validators.required],
      nombreUsuario: ['', Validators.required],
      email: ['', Validators.required],
      roles: [null, Validators.required],
    })

     this.personaService.buscarPersona(this.id).subscribe((resp: BuscarPersonaResponseModel) => {
      // Mapea la respuesta de la API a tu formulario
      this.form.patchValue({
        id: resp.id,
        nombre: resp.nombre,
        nombreUsuario: resp.nombreUsuario,
        email: resp.email,
        roles: resp.roles.map(rol => rol.rolNombre
        ) // Mapea solo los nombres de los roles
      });
      console.log(resp);
    });
  }
}
