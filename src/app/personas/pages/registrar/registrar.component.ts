import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { PersonasService } from '../../services/personas.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Route, Router } from '@angular/router';
import { RegistrarPersonaRequestModel } from '../../interfaces/RegistrarPersonaRequestModel.interface';
import { MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { NuevoUsuario } from '../../interfaces/nuevo-usuario';

@Component({
  selector: 'app-registrar',
  templateUrl: './registrar.component.html',
  styleUrls: ['./registrar.component.css']
})
export class RegistrarComponent implements OnInit {

  form!: FormGroup;
  rols: string[] = ['ADMIN', 'USER'];

  constructor(private http: HttpClient, private personaService: PersonasService, private fb: FormBuilder, private router: Router,private dialogRef: MatDialogRef<RegistrarComponent>,private _snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      nombre: ['', Validators.required],
      nombreUsuario: ['', Validators.required],
      password: ['', Validators.required],
      email: ['', Validators.required]
    })
  }

  registrar() {
    const persona: NuevoUsuario = this.form.value;
    console.log(persona);
    this.personaService.registrarPersona(persona).subscribe(() => {
      this.dialogRef.close(true);
      this.openSnackBar("La persona se registro con exito","aceptar");
      this.form.reset();
    },(error)=>{
      console.log(error)
      this.openSnackBar(error.error.text,"aceptar");
    })
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action,{
      duration: 3000
    });
  }

}
