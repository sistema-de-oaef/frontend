import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/seguridad/services/auth.service';
import { TokenService } from 'src/app/seguridad/services/token.service';

@Component({
  selector: 'app-principal',
  templateUrl: './principal.component.html',
  styleUrls: ['./principal.component.css']
})
export class PrincipalComponent {

  constructor(private tokenService:TokenService,private router:Router){
  }

  cerrarSesion(){
    this.tokenService.logout();
    this.router.navigate(['/login'])
  }
}
