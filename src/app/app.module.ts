import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { MaterialModule } from './material/material.module';
import { interceptorProvider } from './seguridad/services/prod-interceptor.service';
import { AuthGuard } from './seguridad/services/auth-guard.service';
import { ProdGuardService } from './seguridad/services/prod-guard.service';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MaterialModule
  ],
  providers: [interceptorProvider,AuthGuard,ProdGuardService],
  bootstrap: [AppComponent]
})
export class AppModule { }
